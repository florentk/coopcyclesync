#!/usr/bin/python3

import http.client, urllib.parse
import json
import re
import os.path

TOKEN_FILE="coop_cycle_token"
LAST_ID_TASK_FILE="coop_cycle_last_id"

LILLE_BIKE_SERVER="serveur.kaisser.name"
COOP_CYCLE_SERVER="lille.coopcycle.org"

SMS_SERVER="serveur"
SMS_SERVER_PORT=8767

TEL_NOTIFICATION=""

def cc_login(conn, cc_user, cc_passwd):
  print("username: %s" % cc_user)
  params = urllib.parse.urlencode({"_username":cc_user,"_password":cc_passwd})
  headers = {"Accept":"application/ld+json", "Content-type": "application/x-www-form-urlencoded"}
  conn.request("POST", "/api/login_check", params, headers)
  response = conn.getresponse()
  login_json = json.loads(response.read().decode())
  token = login_json['token']
  with open(TOKEN_FILE, "w") as f:
    f.write(token)
  return token
  
def lille_bike_api(conn, key, method):
  conn.request("GET", "/lille.bike/index.php/%s?key=%s&format=json" % (method, key))
  return json.loads(conn.getresponse().read().decode())
  
def coop_cycle_api(conn, token, method):
  params = urllib.parse.urlencode({})
  headers = {"Authorization":"Bearer %s" % (token),"Content-Type":"application/ld+json"}
  conn.request("GET", "/api/%s" % (method), params, headers)
  response = conn.getresponse()
  json_response = json.loads(response.read().decode())
  return (response,json_response)
  
def notify_not_assigned():
  text = "Vous avez des taches a assigner %s" % ("https://lille.coopcycle.org/admin")
  message = {
    "Text" : text,
    "Number" : TEL_NOTIFICATION
  }

  print (text)

  conn = http.client.HTTPConnection(SMS_SERVER, SMS_SERVER_PORT)
  headers = {"Content-Type":"application/json"}
  conn.request("POST", "", json.dumps(message), headers)
  response = conn.getresponse()
  json_response = json.loads(response.read().decode())
  conn.close()
  return (response,json_response)
  
def convert_task_to_livraison(task, next_task, magasins, villes, coursiers):
  id_task = int(task['id'])

  # retrieve the cliend ID from sotre list
  address_pickup= task['address']
  mag = list(filter(lambda m: m['nom'].lower().strip() == address_pickup['name'].lower().strip(),magasins))[0]
  address_delivery = next_task['address']
  
  livr = {}
  
  livr['donneur_ordre'] = mag['mag_id']
  livr['client'] = mag['client_id']
  
  livr['date'] =  re.split('T|\+',task['doneAfter'])[0]
  livr['heure_debut'] = re.split('T|\+',task['doneAfter'])[1]
  livr['heure_fin'] = re.split('T|\+',next_task['doneBefore'])[1]
  livr['heure_min'] = re.split('T|\+',next_task['doneAfter'])[1]

  livr['delai'] = '4'
  livr['attente'] = '0'
  livr['poids'] = '1'
  livr['valeur'] = '0'
  
  livr['destinataire'] = '-1'
  
  if(task['comments']):
    livr['commentaire'] = task['comments']
  else:
    livr['commentaire'] = ""
  
  livr['commande_ref'] = "CC%i" % id_task
  
  # set adresse dest !!
  if(address_delivery['name']):
    livr['nom'] = address_delivery['name']
    livr['prenom'] = ""
  else:
    livr['nom'] = address_delivery['firstName']
    livr['prenom'] = address_delivery['lastName']
  
  sa = address_delivery['streetAddress'].split(',')
  street = sa[0].strip()
  city = sa[1].strip()
  
  if(street[0].isdigit()):
    sb = street.split(' ')
    livr['adr_num'] = sb[0]
    livr['adr_voie'] =  ' '.join(sb[1:])
  else:
    livr['adr_num'] = 0
    livr['adr_voie'] = street
  
  livr['adr_cp_id'] = "-1"
  for cp_id, ville in villes.items():
    if ville.lower().strip() == city.lower().strip():
        livr['adr_cp_id'] = cp_id
  
  if(address_delivery['telephone']):
    livr['tel1'] = address_delivery['telephone']
  else:
    livr['tel1'] = ""
    
  livr['tel2'] = ""
  livr['raison_social'] = ""
  livr['siren'] = ""
  livr['service'] = ""
  
  if(address_delivery['description']):
    livr['adr_cpmt'] = address_delivery['comments']
  else:
    livr['adr_cpmt'] = ""
    
  if(address_delivery['floor']):
    livr['adr_cpmt'] = livr['adr_cpmt'] + "\nEtage : %s" % address_delivery['floor']
  
  livr['coursier'] = "-1"
  assignedTo = task['assignedTo']
  if(assignedTo):
    for coursier_id, coursier in coursiers.items():
      if coursier.lower().strip() == assignedTo.lower().strip():
          livr['coursier'] = coursier_id
  
  return livr

def process_tasks(cc_user, cc_passwd, key):
  token = ""
  
  # connect to CoopCyle
  conn = http.client.HTTPSConnection(COOP_CYCLE_SERVER)

  #try to read token from file
  if (os.path.isfile(TOKEN_FILE)):
    with open(TOKEN_FILE, "r") as f:
      token = f.readline().strip()

  #test token validity
  (response, user) = coop_cycle_api(conn, token, "me")
  #if token not valid, renew token
  if(response.status == 401):
    print ("Renew token")
    token = cc_login(conn, cc_user, cc_passwd)
  elif (response.status == 200): 
    print("username: %s" % (user['username']))

  # get coopcycle current tasks for specific date
  (response, tasks_json) = coop_cycle_api(conn, token, "tasks")
  tasks = tasks_json['hydra:member']

  conn.close()

  # connect to LILLE.BIKE
  conn = http.client.HTTPConnection(LILLE_BIKE_SERVER)

  # get LILLE.BIKE stores list
  magasins = lille_bike_api(conn, key, "courses/magasins")

  # get LILLE.BIKE coursiers list
  coursiers = lille_bike_api(conn, key, "courses/coursiers")

  # get LILLE.BIKE ville list
  villes = lille_bike_api(conn, key, "courses/villes")

  # get all LILLE.BIKE current course to do
  courses = list(filter(lambda c: c['client_ref'][0:2] == 'CC',lille_bike_api(conn, key, "journal_livraison")))

  max_id_task_not_assigned = -1

  # Get only the pickup task to add
  pickups = filter(lambda t: 
      t['type'] == 'PICKUP' and 
      t['status'] == 'TODO'and
      # check if task is not already added
      len(list(filter(lambda c: t['id'] == int(c['client_ref'][2:]), courses))) == 0,
    tasks)

  # add tasks to LILLE.BIKE app
  for task in pickups:
    id_task = int(task['id'])
    
    # retrieve next task
    id_next = int(task['next'].split('/')[-1:][0])
    next_task = list(filter(lambda t: t['id'] == id_next,tasks))[0]
    
    livr = convert_task_to_livraison(task, next_task, magasins, villes, coursiers)
    
    if(livr['coursier'] == "-1"):
      print ("task %d not assigned" % (id_task))
      if(id_task > max_id_task_not_assigned):
        max_id_task_not_assigned = id_task
    else:
      print("add task #%d" % id_task)
      livr['key'] = key
      headers = {"Content-type": "application/json","Accept": "text/plain"}
      conn.request("POST", "/lille.bike/index.php/courses/ajout_livraison", json.dumps(livr),headers)
      rep_html = conn.getresponse().read()
    
  # bye bye LILLE.BIKE
  conn.close()

  # generate notification once
  last_id = -1
  if (os.path.isfile(LAST_ID_TASK_FILE)):
    with open(LAST_ID_TASK_FILE, "r") as f:
      last_id = int(f.readline().strip())

  if(max_id_task_not_assigned > last_id):
    notify_not_assigned()
    with open(LAST_ID_TASK_FILE, "w") as f:
      f.write(str(max_id_task_not_assigned))

def main():
  #CoopCyle login
  cc_user=''
  cc_passwd=''
  #LILLE.BIKE key
  key=''

  process_tasks(cc_user, cc_passwd, key)
  
if __name__ == "__main__":
  main()

